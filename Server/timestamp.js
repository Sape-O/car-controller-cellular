/**
 *  เวลา เอาไว้สำหรับ ให้แสดง ออกทาง Terminal เป็น Time stamp
 * ด้านล่างคือเอาไว้ get เวลา และ นาที
 */
var currentDate = new Date()
var date = currentDate.getDate()
var month = currentDate.getMonth()//Be careful! January is 0 not 1
var year = currentDate.getFullYear()
var dateString = date + "-" +(month + 1) + "-" + year
var hours = currentDate.getHours()
var minutes = currentDate.getMinutes()
var seconds = currentDate.getSeconds()
var milliseconds = currentDate.getMilliseconds()
var timestamp = hours+":"+minutes+":"+seconds+":"+milliseconds
module.exports = {
  dateString,
  timestamp
}
