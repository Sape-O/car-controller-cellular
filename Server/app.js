const path = require('path')
const express = require('express')
const app = express()
const http = require('http').Server(app)
const io  = require('socket.io')(http)
const {dateString,timestamp} = require('./timestamp')




var speed = 50
const HTTP_PORT = 80
//const IP_SERVER = 'https://127.0.0.1'  it self is Server


let RobotId

io.on('connection', (socket) =>{

  socket.on('connectToserver', (name) =>{
    if(name == 'robot'){
      RobotId = socket.id
      io.emit('statusRobot','on')
    }

  })
  if(RobotId !='')  io.emit('statusRobot','on')
  // check ว่า Robot disconnect หรือยัง ถ้า disconnect ให้บอก Client
  socket.on('disconnect', function() {
    if(RobotId == socket.id){
      RobotId =''
      io.emit('statusRobot','off')
      console.log('\nRobot Disconnect Time Stamp : '+timestamp)
    }else console.log('\nClient Disconnect Time Stamp : '+timestamp)


  })



  /*  Car Send speed && Capacity to server
   *  Server send to Client
   *  รอรับ จาก Robot แล้วค่อยส่งต่อไปหา Client
   */
  // socket.on คือ ให้ทาง Robot เป็นคนส่งมาที่ Server
  socket.on('to server Speed', (speed) =>{
    //console.log("Car Send Speed : ",speed)
    // io.emit  คือ Server ส่งไปหา Client
    io.emit('Speed',speed)
  })
  // socket.on คือ ให้ทาง Robot เป็นคนส่งมาที่ Server
  socket.on('to server Capacity', (capacity) =>{
    //console.log("Car Send Capacity : ",capacity)
    // io.emit  คือ Server ส่งไปหา Client
    io.emit('Capacity',capacity)
  })


  /*  Key Press for Control Robot
   *  w = Forword
   *  s = Backword
   *  a = Left
   *  d = Right
   *  q = SpeedDown
   *  e = SpeedUP
   *  u = Servo1Left
   *  i = Servo1Right
   *  j = Servo2Left
   *  k = Servo2Right
   *
   * สำหรับ Client สั่งงาน โดยการกด ปุ่ม keyboard มันจะมาเข้า socket.on
   * จาก นั้น server ก็จะส่งข้อมูล ต่อไปยัง Robot
   * คำสั่งที่ใช้ในการส่งคือ io.emit
   */
  socket.on('keypress', (keypress) =>{
    //console.log("Keypress is : ", keypress)
    if(keypress == 'w' ||
       keypress == 's' ||
       keypress == 'a' ||
       keypress == 'd' ||
       keypress == 'u' ||
       keypress == 'i' ||
       keypress == 'j' ||
       keypress == 'k'
    ){
      io.emit('keypress',{'keypress':keypress,'speed':speed})
    }else if(keypress == 'q'){
      // ลด speed
      if(speed <= 10) speed = 10
      else speed-=10
    }else if(keypress == 'e'){
      // เพิ่ม speed
      if(speed >= 100) speed = 100
      else speed+=10
    }

  })
})





// Page Controller
app.get('/', (req, res) => {

  // ให้แสดงข้อมูล ออกทาง Terminal ว่ามีคน รองขอ หน้าเว็บมา
  console.log("Get Page \"index\" DATE : "+dateString+"  Time Stamp : "+timestamp)
  console.log("IP Client is ->  "+req.connection.remoteAddress)
  console.log("-----------------------------------------------------------------\n")
  // ส่งไฟล์หน้าเว็บ index.html กลับไป
  res.sendFile(path.resolve(__dirname, './public/index.html'))
})


// สำหรับ เรียกใช้งาน javascript เสริม เพื่อใช้ในส่วนแสดงผล วีดีโอ
app.get('/jsmpeg.min.js', (req, res) => res.sendFile(path.resolve(__dirname, './public/jsmpeg.min.js')))

http.listen(HTTP_PORT, () => console.log(`HTTP server listening at http://localhost:${HTTP_PORT}\n`))
