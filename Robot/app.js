const path = require('path')
const express = require('express')
const app = express()
const http = require('http').Server(app)
const io = require('socket.io-client')
// (exports not function) GetSpeed,GetCapacity
//const gsp = require('./module/Get_speed_Capacity')
/*
const {
forward,
backward,
left,
right,
servo1down,
servo1up,
servo2down,
servo2up,
stop
} = require('./Client_Control')
*/
const HTTP_PORT = 8080
const IP_SERVER = 'http://192.168.255.252'

const io_client = io.connect(IP_SERVER)

io_client.emit('connectToserver','robot')

/*
 *  Every 1 Sec Will Send Speed and Capacity
 *

setInterval(()=>{
    /*  Car Send speed && Capacity to server
     *  And then Server will Sent to Client
     *
    io_client.emit('to server Speed',gsp.GetSpeed)
    console.log("Car Send Speed : ",gsp.GetSpeed)
    io_client.emit('to server Capacity',gsp.GetCapacity)
    console.log("Car Send Capacity : ",gsp.GetCapacity)

},1000)

*/
/*  Key Press for Control Robot
 *  w = Forword
 *  s = Backword
 *  a = Left
 *  d = Right
 *  q = SpeedDown
 *  e = SpeedUP
 *  u = Servo1Left
 *  i = Servo1Right
 *  j = Servo2Left
 *  k = Servo2Right

  io_client.on('keypress',req_keypress)
  function req_keypress(key){
    if(key.keypress == 'w') forward(key.speed)
    else if(key.keypress == 's') backward(key.speed)
    else if(key.keypress == 'a') left(key.speed)
    else if(key.keypress == 'd') right(key.speed)
    else if(key.keypress == 'u') servo1down(key.speed)
    else if(key.keypress == 'i') servo1up(key.speed)
    else if(key.keypress == 'j') servo2down(key.speed)
    else if(key.keypress == 'k') servo2up(key.speed)
    else stop()
  }
*/
/* for camera 1 Car send to server and Server sent to Client
 * รับวีดีโอ จาก Robot  แล้วส่งไปยัง client
 */



http.listen(HTTP_PORT, () =>{
  console.log(`HTTP server listening at http://localhost : ${HTTP_PORT}`)
  console.log(`IP Server is : ${IP_SERVER}`)
})
