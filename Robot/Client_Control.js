const Gpio = require('pigpio').Gpio


/**
 *  Left------|- GPIO13, GPIO19  for ให้หมุนซ้าย หรือขวา ปล่อย 0, 1
 *            |- GPIO 26 for PWM
 *
 *  Right-----|- GPIO16, GPIO20 for ให้หมุดซ้าย หรือขวา ปล่อย 0, 1
 *            |- GPIO21 for PWM
 *
 *
 */

// for Left Wheel
const left_1 = new Gpio(13, {mode: Gpio.OUTPUT})
const left_2 = new Gpio(19, {mode: Gpio.OUTPUT})
const pwmLeft = new Gpio(26, {mode: Gpio.OUTPUT})
// for Right Wheel
const right_1 = new Gpio(16, {mode: Gpio.OUTPUT})
const right_2 = new Gpio(20, {mode: Gpio.OUTPUT})
const pwmRight = new Gpio(21, {mode: Gpio.OUTPUT})

let delay = 50


function forward(speed){

  var f = setInterval(() =>{
    //console.log("Robot is Forword -- Speed is :",speed)
    left_1.digitalWrite(1)
    left_2.digitalWrite(0)
    pwmLeft.pwmWrite(speed)

    right_1.digitalWrite(1)
    right_2.digitalWrite(0)
    pwmRight.pwmWrite(speed)

  },0)

  setTimeout(() => {
    left_1.digitalWrite(0)
    left_2.digitalWrite(0)
    pwmLeft.pwmWrite(0)
    right_1.digitalWrite(0)
    right_2.digitalWrite(0)
    pwmRight.pwmWrite(0)

    clearInterval(f)
  },delay)

}

function backward(speed){

  var b = setInterval(() =>{
    //console.log("Robot is backward -- Speed is :",speed)
    left_1.digitalWrite(0)
    left_2.digitalWrite(1)
    pwmLeft.pwmWrite(speed)

    right_1.digitalWrite(0)
    right_2.digitalWrite(1)
    pwmRight.pwmWrite(speed)
  },0)

  setTimeout(() => {
    left_1.digitalWrite(0)
    left_2.digitalWrite(0)
    pwmLeft.pwmWrite(0)
    right_1.digitalWrite(0)
    right_2.digitalWrite(0)
    pwmRight.pwmWrite(0)
    clearInterval(b)
  },delay)

}

function left(speed){
  var l = setInterval(() =>{
    //console.log("Robot is left -- Speed is :",speed)
    left_1.digitalWrite(0)
    left_2.digitalWrite(1)
    pwmLeft.pwmWrite(speed)

    right_1.digitalWrite(1)
    right_2.digitalWrite(0)
    pwmRight.pwmWrite(speed)
  },0)

  setTimeout(() => {
    left_1.digitalWrite(0)
    left_2.digitalWrite(0)
    pwmLeft.pwmWrite(0)
    right_1.digitalWrite(0)
    right_2.digitalWrite(0)
    pwmRight.pwmWrite(0)
    clearInterval(l)
  },delay)
}

function right(speed){
  var r= setInterval(() =>{
    //console.log("Robot is right -- Speed is :",speed)
    left_1.digitalWrite(1)
    left_2.digitalWrite(0)
    pwmLeft.pwmWrite(speed)

    right_1.digitalWrite(0)
    right_2.digitalWrite(1)
    pwmRight.pwmWrite(speed)
  },0)

  setTimeout(() => {
    left_1.digitalWrite(0)
    left_2.digitalWrite(0)
    pwmLeft.pwmWrite(0)
    right_1.digitalWrite(0)
    right_2.digitalWrite(0)
    pwmRight.pwmWrite(0)
    clearInterval(r)
  },delay)

}

/**
 *  เผื่อไว้ สำหรับ มีการใช้ Servo 
 */
var servo1down = (speed) =>{
  console.log("Robot is servo1down -- Speed is :",speed)
}
var servo1up = (speed) =>{
  console.log("Robot is servo1up -- Speed is :",speed)
}
var servo2down = (speed) =>{
  console.log("Robot is servo2down -- Speed is :",speed)
}
var servo2up = (speed) =>{
  console.log("Robot is servo2up -- Speed is :",speed)
}


module.exports = {
  forward,
  backward,
  left,
  right,
  servo1down,
  servo1up,
  servo2down,
  servo2up
}
